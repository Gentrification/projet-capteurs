import serial
from datetime import datetime



def handleFic(liste):
    if liste[0]=="Localisation":
        f.write("      donnees_gps: "+"\n")
        f.write("        - date_donnee: "+date+"\n")
        f.write("          "+liste[1]+": "+liste[2]+"\n")
        f.write("          "+liste[3]+": "+liste[4]+"\n")
    elif liste[0]=="Humidity":
        f.write("      donnees_physique:")
        f.write("        - date_donnee: "+date+"\n")
        f.write("          unite_donnee: %\n")
        f.write("          val_donnee: "+liste[1]+"\n")
        f.write("          nom_donnee: Humidite\n")


ser = serial.Serial('COM3', 115200, timeout = 1)

try:
    while True:
        line = ser.readline()
        if line:
            toTreat = str(line)
            resCaptActu = []
            idArduinoDone = False
            elemActu = ""
            date = str(datetime.now())
            numDossier = str(line)[6:8]+"A"+"_"+date[0:10]+"_"+date[11:13]+"-"+date[14:16]+"-"+date[17:19]+'.yaml'
            f = open(numDossier,'w')
            for i in range(6,len(toTreat)-1):
                if toTreat[i]=="|" :
                    if not idArduinoDone:
                        idArduinoDone = True
                        idArduino = elemActu
                        elemActu = ""
                        
                        f.write("- id_arduino: "+idArduino+"\n")
                        f.write("  capteurs:\n")
                    else:
                        f.write("    - id_capteur: "+elemActu+"\n")
                        elemActu=""
                elif toTreat[i] == "_":
                    resCaptActu.append(elemActu)
                    print(resCaptActu)
                    elemActu = ""
                    handleFic(resCaptActu)
                    resCaptActu=[]

                elif toTreat[i]=="$" or toTreat[i]==";" or toTreat[i]==":":
                    resCaptActu.append(elemActu)
                    elemActu = ""
                else:
                    elemActu+=str(toTreat[i])
                
            f.close()

except KeyboardInterrupt:
    ser.close()
