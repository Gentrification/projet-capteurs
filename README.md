Bio-Capteurs
-----

Ce projet scolaire vise à réaliser un système de récolte, de stockage, d'analyses et d'affichage de données biochimiques  
géolocalisées via une carte Arduino, le réseau 2G, le GPS, une sonde et une application web.


Environnement d'installation
-----
  - Forker et/ou cloner le projet en local
  - Créer un fichier de configuration
  - Créer une variable d'environnement PROJET_CAPTEURS_SETTINGS **système** qui pointe vers ce fichier de configuration
  - Créer un utilisateur puis un schéma dans MySQL qui correspondent à ceux donnés dans le fichier de configuration
  - Créer un environnement virtuel Python 3.x **64 bit** comme virtualenv
  - Activer l'environnement virtuel  
  - Installer les packages nécessaires
  - Initialiser la BD
  - Lancer Flask
  - **VIDER LE CACHE DU NAVIGATEUR**  
  - Créer un compte depuis la page d'inscription et connectez-vous y
  - Peupler la BD

Exemple de processus
-----
  - git clone https://gitlab.com/aurelienbourgerie/projet-capteurs.git
  - cd projet-capteurs (racine du projet)
  - nano settings.cfg 
    - Exemple de config :
        - BOOTSTRAP_SERVE_LOCAL = True
        - SQLALCHEMY_DATABASE_URI = "mysql://root:password@localhost/nom_schema"
        - SECRET_KEY = "7E56A7846-ABU6-4383-0GY1-3RM498L2E46D"
        - SQLALCHEMY_TRACK_MODIFICATIONS = True
        - MAIL_SERVER = "smtp.gmail.com"
        - MAIL_PORT = "465"
        - MAIL_USERNAME = "exemple@gmail.com"
        - MAIL_PASSWORD = "password"
        - MAIL_USE_TLS = False
        - MAIL_USE_SSL = True
  - Pour Windows
    - set PROJET_CAPTEURS_SETTINGS=chemin\vers\settings.cfg
  - Pour Linux et Mac
    - export PROJET_CAPTEURS_SETTINGS=chemin/vers/settings.cfg
  - Si besoin, créer un utilisateur puis un schéma dans MySQL qui correspondent à ceux donnés dans le fichier de configuration
  - virtualenv venv ou virtualenv -p python3 venv ou python -m venv venv
  - source venv/bin/activate ou source venv/Scripts/activate
  - pip install -r requirements.txt
  - flask syncdb
  - flask run
  - **VIDER LE CACHE DU NAVIGATEUR**
  - Créer un compte depuis la page d'inscription
  - S'y connecter depuis la page de connexion
  - flask insertion-donnees web_flask\partie_flask\static\yml\donnee.yml

**Vous pouvez maintenant naviguer sur le site**

Tests
-----

Pour vérifier que votre code passe les tests :

  ./script_test.sh depuis la racine du projet
